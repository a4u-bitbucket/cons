package io.ext;

import com.haulmont.cuba.web.testsupport.TestContainer;

import java.util.ArrayList;
import java.util.Arrays;

public class ConsWebTestContainer extends TestContainer {

    public ConsWebTestContainer() {
        appComponents = Arrays.asList(
                "com.haulmont.cuba",
                "com.haulmont.addon.restapi",
                "com.haulmont.addon.helium",
                "com.haulmont.addon.dashboard",
                "com.haulmont.reports",
                "com.haulmont.addon.globalevents",
                "com.haulmont.addon.admintools",
                "de.diedavids.cuba.dataimport",
                "com.haulmont.fts",
                "com.haulmont.addon.dashboardchart",
                "com.haulmont.addon.emailtemplates",
                "com.haulmont.addon.dnd",
                "com.haulmont.addon.search",
                "com.haulmont.addon.cubaaws",
                "com.haulmont.addon.sdbmt");
        appPropertiesFiles = Arrays.asList(
                // List the files defined in your web.xml
                // in appPropertiesConfig context parameter of the web module
                "io/ext/web-app.properties",
                // Add this file which is located in CUBA and defines some properties
                // specifically for test environment. You can replace it with your own
                // or add another one in the end.
                "com/haulmont/cuba/web/testsupport/test-web-app.properties"
        );
    }

    public static class Common extends ConsWebTestContainer {

        // A common singleton instance of the test container which is initialized once for all tests
        public static final ConsWebTestContainer.Common INSTANCE = new ConsWebTestContainer.Common();

        private static volatile boolean initialized;

        private Common() {
        }

        @Override
        public void before() throws Throwable {
            if (!initialized) {
                super.before();
                initialized = true;
            }
            setupContext();
        }

        @Override
        public void after() {
            cleanupContext();
            // never stops - do not call super
        }
    }
}