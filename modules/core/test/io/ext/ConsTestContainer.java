package io.ext;

import com.haulmont.cuba.testsupport.TestContainer;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.util.ArrayList;
import java.util.Arrays;

public class ConsTestContainer extends TestContainer {

    public ConsTestContainer() {
        super();
        //noinspection ArraysAsListWithZeroOrOneArgument
        appComponents = Arrays.asList(
                "com.haulmont.cuba",
                "com.haulmont.addon.restapi",
                "com.haulmont.addon.helium",
                "com.haulmont.addon.dashboard",
                "com.haulmont.reports",
                "com.haulmont.addon.globalevents",
                "com.haulmont.addon.admintools",
                "de.diedavids.cuba.dataimport",
                "com.haulmont.fts",
                "com.haulmont.addon.dashboardchart",
                "com.haulmont.addon.emailtemplates",
                "com.haulmont.addon.dnd",
                "com.haulmont.addon.search",
                "com.haulmont.addon.cubaaws",
                "com.haulmont.addon.sdbmt");
        appPropertiesFiles = Arrays.asList(
                // List the files defined in your web.xml
                // in appPropertiesConfig context parameter of the core module
                "io/ext/app.properties",
                // Add this file which is located in CUBA and defines some properties
                // specifically for test environment. You can replace it with your own
                // or add another one in the end.
                "io/ext/test-app.properties");
        autoConfigureDataSource();
    }

    

    public static class Common extends ConsTestContainer {

        public static final ConsTestContainer.Common INSTANCE = new ConsTestContainer.Common();

        private static volatile boolean initialized;

        private Common() {
        }

        @Override
        public void beforeAll(ExtensionContext extensionContext) throws Exception {
            if (!initialized) {
                super.beforeAll(extensionContext);
                initialized = true;
            }
            setupContext();
        }
        

        @SuppressWarnings("RedundantThrows")
        @Override
        public void afterAll(ExtensionContext extensionContext) throws Exception {
            cleanupContext();
            // never stops - do not call super
        }
        
    }
}